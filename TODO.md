# ToDo

- [ ] Remove use of RegEx for parsing (this introduced the issue below) and replace with character by character parsing (and proper state machine?)
- [ ] Allow for special characters (-,#, etc. caught by RegEx) in hyperlinks; otherwise, you can't have dashes in hyperlinks!
- [ ] Add support for relative hyperlinks (including markdown files to html)
- [ ] Add support for user input HTML in markdown
- [ ] Add blockquote support
- [ ] Add --- support
- [ ] Add LaTeX support
- [ ] Add footer html
- [ ] Add proper html sections (<section>, <div>, etc.)

## Completed

 - [X] Add code snippets/monospace fonts
 - [X] Add images
 - [X] Add hyperlinks
 - [X] Add bold, italics
 - [X] Add headers
 - [X] Add directory traversal/path generation
 - [X] Add CSS and CSS minification
